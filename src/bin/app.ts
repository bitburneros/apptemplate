import {NS} from "../../index";
import {libApp} from "lib/libapp";

export async function main(ns: NS) {
  await libApp(ns);
}
